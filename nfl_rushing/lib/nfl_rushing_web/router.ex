defmodule NflRushingWeb.Router do
  use NflRushingWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug CORSPlug, origin: ["http://localhost:3000", "http://localhost:5000"]
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", NflRushingWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/players", PlayerController, :index
    get "/players/csv", PlayerController, :export
  end

  # Other scopes may use custom stacks.
   #scope "/api", NflRushingWeb do
     #pipe_through :api
   #end
end
