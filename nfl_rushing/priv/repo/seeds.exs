# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     NflRushing.Repo.insert!(%NflRushing.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
#
alias NflRushing.Players.Player

defmodule NflRushing.Seeds do
  defp parse_lng(%{"Lng" => row_lng} = row) do
    if is_bitstring(row_lng) do
     row 
    else
      Map.put(row, "Lng", Integer.to_string(row_lng))
    end
  end

  defp parse_yds(%{"Yds" => row_yds} = row) do
    if is_number(row_yds) do
      row
    else
      yds = try do
        String.to_float(row_yds)
      rescue
        ArgumentError -> String.to_integer(row_yds)
      end
      Map.put(row, "Yds", yds)
    end
  end

  def parse_row(row) do
    parse_yds(row) |> parse_lng
  end
end


{:ok, data}      = File.read("./priv/repo/rushing.json")
{:ok, json_data} = Poison.decode(data)
Enum.each(
  json_data,
  fn val ->
    %Player{}
    |> Player.changeset(NflRushing.Seeds.parse_row(val))
    |> NflRushing.Repo.insert!
  end
)
