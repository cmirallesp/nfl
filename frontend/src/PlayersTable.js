import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useTable, useSortBy, useFilters, usePagination } from "react-table";
import axios from "axios";
import fileDownload from "js-file-download";
import Pagination from "./Pagination.js";

const PaginationContainer = styled.div`
  padding: 0.5rem;
`;

const Styles = styled.div`
  padding: 1rem;

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }
`;

const Button = styled.button`
  margin: 0.5rem;
  width: 120px;
  height: 30px;
  background-color: #39a1f4;
  color: #fff;
  border: 0;
  border-radius: 2px;
  font-size: 0.875rem;
  letter-spacing: 0.03em;
`;
const Container = styled.div`
  display: flex;
  justify-content: center;
`;

const DefaultColumnFilter = ({
  column: { filterValue, preFilteredRows, setFilter }
}) => {
  const count = preFilteredRows.length;

  return (
    <input
      value={filterValue || ""}
      onChange={e => {
        setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
      }}
      placeholder={`Search ${count} records...`}
    />
  );
};

const TableHeader = props => (
  <thead>
    {props.headerGroups.map(headerGroup => (
      <tr {...headerGroup.getHeaderGroupProps()}>
        {headerGroup.headers.map(column => (
          <th {...column.getHeaderProps(column.getSortByToggleProps())}>
            {column.render("Header")}
            <span>
              {column.isSorted ? (column.isSortedDesc ? " ↓" : " ↑") : ""}
            </span>
            <div>{column.canFilter ? column.render("Filter") : null}</div>
          </th>
        ))}
      </tr>
    ))}
  </thead>
);

const CSVButton = () => (
  <Button
    onClick={() =>
      axios
        .get("http://localhost:4000/players/csv")
        .then(({ data }) => fileDownload(data, "players.csv"))
    }
  >
    Export to CSV
  </Button>
);

const Table = ({ columns, data }) => {
  const defaultColumn = React.useMemo(
    () => ({
      Filter: DefaultColumnFilter
    }),
    []
  );

  const {
    getTableProps,
    headerGroups,

    prepareRow,
    page, // Instead of using 'rows', we'll use page,
    // which has only the rows for the active page

    // The rest of these things are super handy, too ;)
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: [{ pageIndex, pageSize }]
  } = useTable(
    {
      columns,
      data,
      defaultColumn
    },
    useFilters,
    useSortBy,
    usePagination
  );

  return (
    <>
      <table {...getTableProps()}>
        <TableHeader headerGroups={headerGroups}></TableHeader>
        <tbody>
          {page.map(
            (row, i) =>
              prepareRow(row) || (
                <tr {...row.getRowProps()}>
                  {row.cells.map(cell => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                    );
                  })}
                </tr>
              )
          )}
        </tbody>
      </table>
      <PaginationContainer>
        <Pagination
          canPreviousPage={canPreviousPage}
          canNextPage={canNextPage}
          length={pageOptions.length}
          pageCount={pageCount}
          gotoPage={gotoPage}
          nextPage={nextPage}
          previousPage={previousPage}
          setPageSize={setPageSize}
          pageIndex={pageIndex}
          pageSize={pageSize}
        />
        <CSVButton />
      </PaginationContainer>
    </>
  );
};

const PlayersTable = () => {
  const columns = React.useMemo(
    () => [
      {
        Header: "Player",
        accessor: "Player",
        disableSorting: true
      },
      {
        Header: "Team",
        accessor: "Team",
        disableSorting: true,
        disableFilters: true
      },
      {
        Header: "Pos",
        accessor: "Pos",
        disableSorting: true,
        disableFilters: true
      },
      {
        Header: "Att",
        accessor: "Att",
        disableSorting: true,
        disableFilters: true
      },
      {
        Header: "Att/G",
        accessor: "Att/G",
        disableSorting: true,
        disableFilters: true
      },
      {
        Header: "Yrds",
        accessor: "Yds",
        disableFilters: true
      },
      {
        Header: "Avg",
        accessor: "Avg",
        disableSorting: true,
        disableFilters: true
      },
      {
        Header: "Yds/G",
        accessor: "Yds/G",
        disableSorting: true,
        disableFilters: true
      },
      {
        Header: "TD",
        accessor: "TD",
        disableFilters: true
      },

      {
        Header: "Lng",
        accessor: "Lng",
        disableFilters: true
      },
      {
        Header: "1st",
        accessor: "1st",
        disableSorting: true,
        disableFilters: true
      },
      {
        Header: "1st%",
        accessor: "1st%",
        disableSorting: true,
        disableFilters: true
      },
      {
        Header: "20+",
        accessor: "20+",
        disableSorting: true,
        disableFilters: true
      },
      {
        Header: "40+",
        accessor: "40+",
        disableSorting: true,
        disableFilters: true
      },
      {
        Header: "FUM",
        accessor: "FUM",
        disableSorting: true,
        disableFilters: true
      }
    ],
    []
  );

  const [info, setInfo] = useState({ players: [], loaded: false });
  useEffect(() => {
    if (!info.loaded)
      axios
        .get("http://localhost:4000/players")
        .then(({ data: { players } }) => {
          setInfo({ players, loaded: true });
        });
  });

  if (!info.loaded) return "Loading...";
  return (
    <Container>
      <Styles>
        <Table columns={columns} data={info.players} />
      </Styles>
    </Container>
  );
};

export default PlayersTable;
