defmodule NflRushingWeb.PlayerView do
  use NflRushingWeb, :view
  alias NflRushingWeb.PlayerView

  def render("index.json", %{players: players}) do
    %{players: render_many(players, PlayerView, "player.json")}
  end

  def render("show.json", %{player: player}) do
    %{player: render_one(player, PlayerView, "player.json")}
  end

  def render("player.json", %{player: player}) do
    %{    
      "1st"    => player."1st",
      "1st%"   => player."1st%",
      "20+"    => player."20+",
      "40+"    => player."40+",
      "Att"    => player."Att",
      "Att/G"  => player."Att/G",
      "Avg"    => player."Avg",
      "FUM"    => player."FUM",
      "Lng"    => player."Lng",
      "Player" => player."Player",
      "Pos"    => player."Pos",
      "TD"     => player."TD",
      "Team"   => player."Team",
      "Yds"    => player."Yds",
      "Yds/G"  => player."Yds/G"
    }
  end
end
