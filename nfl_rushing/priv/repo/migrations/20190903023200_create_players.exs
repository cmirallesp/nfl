defmodule NflRushing.Repo.Migrations.CreatePlayers do
  use Ecto.Migration

  def change do
    create table(:players) do
      add :"1st", :integer
      add :"1st%", :float
      add :"20+", :integer
      add :"40+", :integer
      add :Att, :integer
      add :"Att/G", :float
      add :Avg, :float
      add :FUM, :integer
      add :Lng, :string
      add :Player, :string
      add :Pos, :string
      add :TD, :integer
      add :Team, :string
      add :Yds, :float
      add :"Yds/G", :float

      timestamps()
    end

  end
end
