defmodule NflRushing.Players.Player do
  use Ecto.Schema
  import Ecto.Changeset

  schema "players" do
    field :"1st", :integer
    field :"1st%", :float
    field :"20+", :integer
    field :"40+", :integer
    field :Att, :integer
    field :"Att/G", :float
    field :Avg, :float
    field :FUM, :integer
    field :Lng, :string
    field :Player, :string
    field :Pos, :string
    field :TD, :integer
    field :Team, :string
    field :Yds, :float
    field :"Yds/G", :float

    timestamps()
  end

  @doc false
  def changeset(player, attrs) do
    player
    |> cast(attrs, [:"1st", :"1st%", :"20+", :"40+", :Att, :"Att/G", :Avg, :FUM, :Lng, :Player, :Pos, :TD, :Team, :Yds, :"Yds/G"])
    #|> validate_required([:"1st", :"1st%", :"20+", :"40+", :Att, :"Att/G", :Avg, :FUM, :Lng, :Player, :Pos, :TD, :Team, :Yds, :"Yds/G"])
  end
end
