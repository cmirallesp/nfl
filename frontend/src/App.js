import React from "react";
import "./App.css";
import PlayersTable from "./PlayersTable";
import logo from "./logo2.jpeg"
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo"  />
      </header> 
      <PlayersTable />
    </div>
  );
}

export default App;
